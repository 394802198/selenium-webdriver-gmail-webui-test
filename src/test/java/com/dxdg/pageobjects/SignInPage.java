package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Steven on 2015/5/24.
 */
public class SignInPage {
    public void fillUsername(WebDriver driver, String s) {
        WebUtil.clearAndSendKeys(driver, By.id("Email"), s);
    }

    public void fillPassword(WebDriver driver, String s) {
        if(WebUtil.isElementDisplayed(driver, By.id("next"))){
            WebUtil.click(driver, By.id("next"));
        }
        WebUtil.waitForElementVisible(driver, By.id("Passwd"));

        WebUtil.clearAndSendKeys(driver, By.id("Passwd"), s);
    }

    public EmailHomePage clickSignIn(WebDriver driver) {
        WebUtil.click(driver, By.id("signIn"));

        // 5. verify user did sign in
        WebUtil.waitForElementVisible(driver, By.partialLinkText("收件箱"));

        return PageFactory.initElements(driver, EmailHomePage.class);
    }

    public boolean isSignInButtonExist(WebDriver driver) {

        return WebUtil.isElementExist(driver, By.id("signIn"));
    }
}
