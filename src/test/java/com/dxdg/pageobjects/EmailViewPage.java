package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Steven on 2015/5/24.
 */
public class EmailViewPage {

    public String getEmailSubjectText(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.cssSelector("h2[class='hP']"));

        return WebUtil.getElementText(driver, By.cssSelector("h2[class='hP']"));
    }

    public String getEmailBodyText(WebDriver driver) {
        return WebUtil.getElementText(driver, By.cssSelector("div[class='adn ads'] div[dir='ltr']"));
    }
}
