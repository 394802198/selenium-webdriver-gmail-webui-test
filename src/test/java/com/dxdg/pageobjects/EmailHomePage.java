package com.dxdg.pageobjects;

import com.dxdg.util.WebUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Steven on 2015/5/24.
 */
public class EmailHomePage {

    public SignInPage signOut(WebDriver driver) {

        WebUtil.click(driver, By.cssSelector("a[class='gb_ga gb_l gb_r gb_h']"));

        WebUtil.waitForElementVisible(driver, By.id("gb_71"));

        WebUtil.click(driver, By.id("gb_71"));

        return PageFactory.initElements(driver, SignInPage.class);
    }

    public boolean isInboxExist(WebDriver driver) {
        return WebUtil.isElementExist(driver, By.partialLinkText("收件箱"));
    }

    public void clickComposeButton(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.cssSelector("div[gh='cm']"));

        WebUtil.click(driver, By.cssSelector("div[gh='cm']"));
    }

    public void fillInRecipient(WebDriver driver, String s) {
        WebUtil.waitForElementVisible(driver, By.cssSelector("textarea[name='to']"));

        WebUtil.clearAndSendKeys(driver, By.cssSelector("textarea[name='to']"), s);
    }

    public void fillInEmailSubject(WebDriver driver, String subject) {
        WebUtil.clearAndSendKeys(driver, By.cssSelector("input[name='subjectbox']"), subject);
    }

    public void fillInEmailBody(WebDriver driver, String body) {
        WebUtil.clearAndSendKeys(driver, By.cssSelector("div[aria-label='邮件正文']"), body);
    }

    public void clickSendEmail(WebDriver driver) {
        WebUtil.click(driver, By.cssSelector("div[aria-label*='发送']"));
    }

    public void clickInboxWithNewEmail(WebDriver driver, String s) {
        WebUtil.waitForElementVisible(driver, By.partialLinkText(s));

        WebUtil.click(driver, By.partialLinkText(s));
    }

    public EmailViewPage clickNewEmail(WebDriver driver) {
        WebUtil.waitForElementVisible(driver, By.cssSelector("td[class='xY a4W'] span b"));

        WebUtil.click(driver, By.cssSelector("td[class='xY a4W'] span b"));

        return PageFactory.initElements(driver, EmailViewPage.class);
    }
}
