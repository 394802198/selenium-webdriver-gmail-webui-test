import com.dxdg.categories.Critical;
import com.dxdg.categories.Major;
import com.dxdg.pageobjects.EmailHomePage;
import com.dxdg.pageobjects.EmailViewPage;
import com.dxdg.pageobjects.SignInPage;
import com.dxdg.util.WebUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Steven on 2015/5/24.
 */
public class GmailSignInTest {

    WebDriver driver;

    @Before
    public void setDriver(){
        String browserName = System.getenv("browser");
        if(browserName!=null && browserName.equalsIgnoreCase("Chrome")){
            driver = new ChromeDriver();
        } else {
            driver = new FirefoxDriver();
        }
    }

    @Category({Critical.class})
    @Test
    public void gmailLoginShouldBeSuccessful(){
        // 1. Go to Gmail website
        SignInPage signInPage = WebUtil.goToSignInPage(driver);
        // 2. Fill in username
        signInPage.fillUsername(driver, "wdtesting88@gmail.com");

        // 3. Fill in password
        signInPage.fillPassword(driver, "wdtesting88123");
        // 4. click sign in
        EmailHomePage emailHomePage = signInPage.clickSignIn(driver);

        Assert.assertTrue("Inbox should exist", emailHomePage.isInboxExist(driver));
        // 6. sign out
        signInPage = emailHomePage.signOut(driver);

        // 7. verified user did sign out
        Assert.assertTrue("signIn button should exist", signInPage.isSignInButtonExist(driver));

    }

    @Category({Major.class})
    @Test
    public void gmailSendAndReceiveEmailTest(){
        //  1.  Click sign in
        // 1.1. Go to Gmail website
        SignInPage signInPage = WebUtil.goToSignInPage(driver);
        // 1.2. Fill in username
        signInPage.fillUsername(driver, "wdtesting88@gmail.com");

        // 1.3. Fill in password
        signInPage.fillPassword(driver, "wdtesting88123");
        // 1.4. click sign in
        EmailHomePage emailHomePage = signInPage.clickSignIn(driver);

        //  2.  Click Compose
        emailHomePage.clickComposeButton(driver);
        //  3.  Fill in recipient
        emailHomePage.fillInRecipient(driver, "wdtesting88@gmail.com");
        //  4.  Fill in subject
        final String subject = "Gmail Send Email Test";
        emailHomePage.fillInEmailSubject(driver, subject);
        //  5.  Fill in email body
        final String body = "Hello Testers Good Morning";
        emailHomePage.fillInEmailBody(driver, body);
        //  6.  Click Send
        emailHomePage.clickSendEmail(driver);
        //  7.  Click Inbox again
        emailHomePage.clickInboxWithNewEmail(driver, "收件箱 (");
        //  8.  Click email
        EmailViewPage emailViewPage = emailHomePage.clickNewEmail(driver);
        //  9.  Verify the email subject and email body is correct
        String actualSubject = emailViewPage.getEmailSubjectText(driver);
        Assert.assertEquals("Email Subject Text should be the same", subject, actualSubject);
        String actualBody = emailViewPage.getEmailBodyText(driver);
        Assert.assertEquals("Email Body Text should be the same", body, actualBody);
        //  10.  Sign out
        emailHomePage.signOut(driver);

        // 7. verified user did sign out
        Assert.assertTrue("signIn button should exist", signInPage.isSignInButtonExist(driver));

    }

    @After
    public void tearDown(){
        driver.quit();
    }
}





